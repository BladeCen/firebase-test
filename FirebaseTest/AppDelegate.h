//
//  AppDelegate.h
//  FirebaseTest
//
//  Created by Blade on 2018/10/26.
//  Copyright © 2018 Joyrun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


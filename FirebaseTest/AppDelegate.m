//
//  AppDelegate.m
//  FirebaseTest
//
//  Created by Blade on 2018/10/26.
//  Copyright © 2018 Joyrun. All rights reserved.
//

#import "AppDelegate.h"
@import Firebase;
@import Fabric;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self initFirebase];
    [self initFabric];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



#pragma mark -
- (void)initFirebase {
    FIRRemoteConfig *remoteConfig = [FIRRemoteConfig remoteConfig];
    NSString *performanceRCKey = @"performance_enabled";
#ifdef DEBUG
    FIRRemoteConfigSettings *settings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:YES];
    remoteConfig.configSettings = settings;
#endif
    [remoteConfig setDefaults:@{performanceRCKey:@"false"}];
    [remoteConfig activateFetched]; // Active the last fectched config.
    FIRRemoteConfigValue *value = [remoteConfig configValueForKey:performanceRCKey];
    BOOL shouldEnablePerformance = value && [value.stringValue isEqualToString:@"true"];
    if (shouldEnablePerformance) {
        [FIRPerformance sharedInstance].dataCollectionEnabled = YES;
        [FIRPerformance sharedInstance].instrumentationEnabled = YES;
    }
    else {
        // Ref: https://firebase.google.com/docs/perf-mon/disable-sdk
        // The following line disables automatic traces and HTTP/S network monitoring
        [FIRPerformance sharedInstance].dataCollectionEnabled = NO;
        // The following line disables custom traces
        [FIRPerformance sharedInstance].instrumentationEnabled = NO;
    }
    // Fetch new config from the server.
    [remoteConfig fetchWithCompletionHandler:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
        //    [remoteConfig fetchWithExpirationDuration:24 * 60 * 60 completionHandler:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
        NSLog(@"%@", remoteConfig);
    }];
//    [FIRApp configure];
#ifdef DEBUG
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result, NSError * _Nullable error) {
        NSLog(@"FIRInstanceID token %@", result.token);
    }];
#endif
    [FIRApp configure];
}

- (void)initFabric {
    [Fabric with:@[[Crashlytics class], [Answers class]]];
}
@end

//
//  ViewController.m
//  FirebaseTest
//
//  Created by Blade on 2018/10/26.
//  Copyright © 2018 Joyrun. All rights reserved.
//

#import "ViewController.h"
#import <AMapSearchKit/AMapSearchAPI.h>
#import <AMapFoundationKit/AMapFoundationKit.h>

@interface ViewController () <CLLocationManagerDelegate, AMapSearchDelegate>
@property (nonatomic, strong) AMapSearchAPI *aMapSearchAPI;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.locationManager = [[CLLocationManager alloc] init];
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
    
    self.aMapSearchAPI = [[AMapSearchAPI alloc] init];
    self.aMapSearchAPI.delegate = self;
    
    
    
    [self startUpdatingLocation];
}

- (void) startUpdatingLocation {
    [self.locationManager setDelegate:self];
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    [self updateLocationInfoWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
}

- (void)updateLocationInfoWithLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude {
    AMapReGeocodeSearchRequest *regeoRequest = [[AMapReGeocodeSearchRequest alloc] init];
//    regeoRequest.done
    regeoRequest.location = [AMapGeoPoint locationWithLatitude:latitude longitude:longitude];
    [self.aMapSearchAPI AMapReGoecodeSearch:regeoRequest];
}

#pragma mark - AMapSearchDelegate
#pragma mark -
- (void)AMapSearchRequest:(AMapReGeocodeSearchRequest *)request didFailWithError:(NSError *)error {
    
}

- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response {
    AMapAddressComponent *component = response.regeocode.addressComponent;
    AMapGeoPoint *location = request.location;
    [self.locationManager stopUpdatingLocation];
//    AddressServiceDidDidUpdateBlock block = request.doneBlock;
//
//    [self handleReverseGeoCodingWithAmapResult:location addressComponent:component finishBlock:^(JRCurAddress *handledAddress, NSError *handleError) {
//        if (handledAddress) {
//            [self updateCurAddressWith:handledAddress];
//        }
//
//        if (block) {
//            block(handledAddress, handleError);
//        }
//    }];
}

@end
